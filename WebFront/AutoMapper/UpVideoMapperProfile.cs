﻿using AutoMapper;
using Frontdatabase.Entity.Video;
using Model.UpVideoModel;

namespace WebFront.AutoMapper
{
    /// <summary>
    /// 上传投稿视频时 前端表单和efcore实体类之间的映射关系
    /// </summary>
    public class UpVideoMapperProfile : Profile
    {
        public UpVideoMapperProfile()
        {
            CreateMap<UploadVideoMergeform, VideoEntity>(); 
        }
    }
}

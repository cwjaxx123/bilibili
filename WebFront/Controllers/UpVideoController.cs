﻿using Common.Helper;
using Frontdatabase.Entity.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.UpVideoModel;
using Newtonsoft.Json;
using WebFront.Service;

namespace WebFront.Controllers
{
    /// <summary>
    /// 上传投稿视频接口
    /// </summary>
    [ApiController, Route("api/[controller]/[action]"), Authorize]
    public class UpVideoController(IUpVideoService _upVideoService) : ControllerBase
    {

        /// <summary>
        /// 获取所有的视频类别和分区
        /// </summary>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<ActionResult<ResResult>> CategoryAndSubarea() => await _upVideoService.CategoryAndSubarea();

        /// <summary>
        /// 上传投稿视频的表单 
        /// </summary>
        /// <param name="_form"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ResResult>> UploadFrom([FromBody] UploadVideoForm _form)
        {
            var claims = JwtHelper.GetInfoByClaims(Request, [nameof(EntityBase.Id)]);
            if (claims.Count == 0) return BadRequest("未存在登录用户");
            Guid userId = Guid.Parse( claims[nameof(EntityBase.Id)]);
            _form.UserId = userId;
            var res = await _upVideoService.UploadFrom(_form);
            return res;
        }

        /// <summary>
        /// 上传视频封面切片
        /// </summary>
        /// <param name="videoCoverChunk"></param>
        /// <param name="cover"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ResResult>> UploadCoverChunk([FromQuery] UpVideoCoverChunkForm videoCoverChunk, [FromForm] IFormFile cover)
                                                => await _upVideoService.UploadCoverChunkSave(videoCoverChunk, cover);

        /// <summary>
        /// 合并上传的封面切片
        /// </summary>
        /// <param name="FormMd5"></param>
        /// <param name="ChunkCount"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ResResult>> UploadMerginCover(UpVideoMerginCoverForm _upVideoMerginCoverForm)
                                                    => await _upVideoService.UploadMerginCoverSave(_upVideoMerginCoverForm);
        /// <summary>
        /// 上传视频切片文件接口
        /// </summary>
        /// <param name="VideoChunk"></param>
        /// <param name="_uploadVideoFileChunkForm"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ResResult>> UploadVideoChunk(IFormFile VideoChunk, [FromQuery] UploadVideoFileChunkForm _uploadVideoFileChunkForm)
                                                    => await _upVideoService.UploadVideoChunkSave(VideoChunk, _uploadVideoFileChunkForm);

        /// <summary>
        /// 合并上传视频切片接口
        /// </summary>
        /// <param name="_uploadVideoMergeform"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ResResult>> UploadMerginVideo(UploadVideoMergeform _uploadVideoMergeform)
                                    => await _upVideoService.UploadMerginVideoSave(_uploadVideoMergeform);


    }
}

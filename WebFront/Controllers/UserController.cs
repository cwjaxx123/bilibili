﻿using Common;
using Common.Filter;
using Frontdatabase;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Model;
using Model.UserModel;
using System.IdentityModel.Tokens.Jwt;
using WebFront.Service;

namespace WebFront.Controllers
{
    /// <summary>
    /// 用户对外相关接口类
    /// </summary>
    [ApiController, Route("api/[controller]/[action]"), Authorize]
    public class UserController(IUserSevice userSevice) : ControllerBase
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous, TypeFilter(typeof(ModelValidationFilter))]
        public async Task<ActionResult<ResResult>> Login([FromForm] UserLoginModel _userLoginModel) => await userSevice.Login(_userLoginModel);

    }
}

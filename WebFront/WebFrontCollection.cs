﻿
using WebFront.Service;
using WebFront.Service.Impl;

namespace WebFront
{
    public static class WebFrontCollection
    {
        /// <summary>
        /// 注入WebFront_Service的服务对象进ioc容器
        /// <para>控制器controller已经自动注册进容器,不用手动注册</para>
        /// <para>注册service服务 和 其他中间件对象</para>
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddWebFrontService(this IServiceCollection services)
        {
           services.AddScoped<IUserSevice,UserSeviceImpl>();
            services.AddScoped<IUpVideoService,UpVideoServiceImpl>();
            return services;
        }
        /// <summary>
        /// 加入弹幕websocket中间件
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseDanmuWebSocket(this IApplicationBuilder app)
        {
            return app;
        }
    }
}

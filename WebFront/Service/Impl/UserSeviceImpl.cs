﻿using Common.Helper;
using Frontdatabase;
using Frontdatabase.Entity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Model;
using Model.Enum;
using Model.UserModel;
using System.Security.Claims;
using System.Security.Cryptography;

namespace WebFront.Service.Impl
{
    public class UserSeviceImpl(FrontDbContext _frontDbContext, JwtHelper _jwtHelper) : IUserSevice
    {
        /// <summary>
        /// 用户登录业务实现
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ResResult> Login(UserLoginModel _usermodel)
        {
            //根据登录方式 动态设置linq
            var query = _usermodel.LoginMethod switch
            {
                LoginMethodEnum.Phone => _frontDbContext.Users.Where(u => u.Phone == _usermodel.Phone),
                LoginMethodEnum.Email => _frontDbContext.Users.Where(u => u.Email == _usermodel.Email),
                _ => _frontDbContext.Users
            };
            User user = await query.Where(u => u.Password == MD5Helper.ToMd5Str(_usermodel.Password, null)).FirstOrDefaultAsync();

            if (user is null) return new ResResult(null!, ResultEnum.Error, ErrorCodeEnum.用户不存在);

            //用户信息
            var claims = new Claim[]
              {
               new( nameof(user.Id), user.Id.ToString() ),
               new(nameof(user.Grade),user.Grade.ToString()),
              };
            //生成jwt
            var token = _jwtHelper.CreateToken(claims);
            return new ResResult(new
            {
                token = JwtBearerDefaults.AuthenticationScheme + " " + token,
                user.Profile,
                loginTime = DateTime.Now,
            });
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Model.UserModel;

namespace WebFront.Service
{
    /// <summary>
    /// 用户接口
    /// </summary>
    public interface IUserSevice
    {
        /// <summary>
        /// 用户登录业务
        /// </summary>
        Task<ResResult> Login(UserLoginModel _usermodel); 
    }
}

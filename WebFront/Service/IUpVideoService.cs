﻿using Front_Efcore.Entity.subarea;
using Model;
using Model.UpVideoModel;

namespace WebFront.Service
{
    /// <summary>
    /// 上传投稿视频业务接口
    /// </summary>
    public interface IUpVideoService
    {
        /// <summary>
        /// 获取类别集合
        /// 类别-分区
        /// </summary>
        /// <returns></returns>
         Task<ResResult> CategoryAndSubarea();
        /// <summary>
        /// 上传视频表单接口
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        Task<ResResult> UploadFrom(UploadVideoForm form);
        /// <summary>
        /// 保存视频封面切片
        /// <para>存储redis</para>
        /// </summary>
        /// <param name="videoCoverChunk"></param>
        /// <param name="cover"></param>
        /// <returns></returns>
        Task<ResResult> UploadCoverChunkSave(UpVideoCoverChunkForm videoCoverChunk, IFormFile cover);
        /// <summary>
        /// 合并上传的视频封面 并存储到服务器本地
        /// <para>删除redis中的数据 和 服务器本地封面切片文件</para>
        /// 
        /// </summary>
        /// <param name="FormMd5"></param>
        /// <param name="ChunkCount"></param>
        /// <returns></returns>
        Task<ResResult> UploadMerginCoverSave(UpVideoMerginCoverForm _upVideoMerginCoverForm);
        /// <summary>
        /// 保存上传视频切片文件
        ///  <para>存储redis</para>
        /// </summary>
        /// <param name="VideoChunk"></param>
        /// <param name="_uploadVideoFileChunkForm"></param>
        /// <returns></returns>
        Task<ResResult> UploadVideoChunkSave(IFormFile VideoChunk, UploadVideoFileChunkForm _uploadVideoFileChunkForm);
        /// <summary>
        /// 合并上传的视频切片
        /// <para>并删除redis数据</para>
        /// </summary>
        /// <param name="_uploadVideoMergeform"></param>
        /// <returns></returns>
        Task<ResResult> UploadMerginVideoSave(UploadVideoMergeform _uploadVideoMergeform);
    }
}

﻿namespace Admin
{
    public static class AdminServiceCollection
    {
        public static IServiceCollection AddAdmin_Service(this IServiceCollection services)
        {
            return services;
        }
        public static IApplicationBuilder UseAdminMiddle(this IApplicationBuilder app)
        {
            return app;
        }
    }
}

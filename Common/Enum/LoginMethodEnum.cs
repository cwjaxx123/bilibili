﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    /// <summary>
    /// 用户登录方式枚举
    /// </summary>
    public enum LoginMethodEnum
    {
        /// <summary>
        /// 手机号登录
        /// </summary>
        Phone,
        /// <summary>
        /// 邮箱登录
        /// </summary>
        Email,
        /// <summary>
        /// 手机短信验证码登录
        /// </summary>
        PhoneNote,
        /// <summary>
        /// 邮箱验证码登录
        /// </summary>
        EmailNote,
        /// <summary>
        /// 微信登录
        /// </summary>
        Wechat,
        /// <summary>
        /// qq登录
        /// </summary>
        QQChat,
        /// <summary>
        /// 微博登录
        /// </summary>
        WeiBo,
    }
}

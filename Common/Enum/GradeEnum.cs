﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontdatabase.Enum
{
    /// <summary>
    /// 用户等级
    /// </summary>
    public enum GradeEnum
    {
        /// <summary>
        /// 0级
        /// </summary>
        Zeroth,
        /// <summary>
        /// 1级
        /// </summary>
        Firest,
        /// <summary>
        /// 2级
        /// </summary>
        Second,
        /// <summary>
        /// 3级
        /// </summary>
        Thirdly,
        /// <summary>
        /// 4级
        /// </summary>
        Fourthly,
        /// <summary>
        /// 5级
        /// </summary>
        Fifth,
        /// <summary>
        /// 6级
        /// </summary>
        Sixth
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    /// <summary>
    /// 返回状态码枚举
    /// <para>
    /// todo 为了和http响应状态码区分开来 ,要设置为600以上
    /// </para>
    /// </summary>
    public enum ResultEnum
    {
        /// <summary>
        /// 正常调用
        /// </summary>
        Suceess = 2000,
        /// <summary>
        /// 状态异常
        /// </summary>
        Fail = 2001,

        Error = 20002,
        NotFindError = 6002
    }
}

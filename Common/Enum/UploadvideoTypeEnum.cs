﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enum
{
    /// <summary>
    /// 投稿视频来源
    /// </summary>
    public enum UploadvideoTypeEnum
    {
        /// <summary>
        /// 自制
        /// </summary>
        self,
        /// <summary>
        /// 转载
        /// </summary>
        source
    }
}

﻿using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Model;
using Model.Enum;


namespace Common.Filter;
/// <summary>
/// 输入的表单验证 过滤器
/// </summary>
public class ModelValidationFilter : IAsyncActionFilter
{
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var arguments = context.ActionArguments.Values;
        foreach (var argument in arguments)
        {
            if (argument is IModelValidationInterface intfa)
            {
                if (intfa.IsVerify) continue;
                var res = new ActionResult<ResResult>(new ResResult(null!, ResultEnum.Error, ErrorCodeEnum.表单验证不通过));
                //直接返回结果
                context.Result = new JsonResult(res);
            }
            else if (argument is string str)
            {
                if (!string.IsNullOrWhiteSpace(str)) continue;
                var res = new ActionResult<ResResult>(new ResResult(null!, ResultEnum.Error, ErrorCodeEnum.表单验证不通过));
                //直接返回结果
                context.Result = new JsonResult(res);
            }
        }
        await next();
    }
}


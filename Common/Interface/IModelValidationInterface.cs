﻿namespace Common;

/// <summary>
/// 表单验证接口
/// </summary>
public interface IModelValidationInterface
{
    /// <summary>
    /// 表单验证标志
    /// </summary>
    bool IsVerify { get; }
}


﻿using StackExchange.Redis;
using System.Collections.Concurrent;


namespace Common.Helper
{
    /// <summary>
    /// Redis管理
    /// </summary>
    /// <param name="connectionString"></param>
    /// <param name="instanceName"></param>
    /// <param name="defaultDb"></param>
    public class RedisHelper(string connectionString, string instanceName, int defaultDb = 0)
    {
        private readonly ConcurrentDictionary<string, ConnectionMultiplexer> _connections = [];

        /// <summary>
        /// 获取ConnectionMultiplexer
        /// </summary>
        /// <returns></returns>
        private ConnectionMultiplexer GetConnect()
        {
            return _connections.GetOrAdd(instanceName, p => ConnectionMultiplexer.Connect(connectionString));
        }

        /// <summary>
        /// 获取数据库
        /// </summary>
        /// <param name="configName"></param>
        /// <param name="db">默认为0：优先代码的db配置，其次config中的配置</param>
        /// <returns></returns>
        public IDatabase GetDatabase()
        {
            return GetConnect().GetDatabase(defaultDb);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helper
{
    /// <summary>
    /// md5工具类
    /// <para>不能实例化和继承</para>
    /// <para>只能使用静态方法</para>
    /// </summary>
    public static class MD5Helper
    {
        /// <summary>
        /// 明文字符串转MD5加密字符串
        /// <para>通常用来加密客户端密码</para>
        /// </summary>
        /// <param name="input">明文字符串</param>
        /// <param name="salt">盐 (加盐可以降低被穷举破解的可能性)</param>
        /// <returns></returns>
        public static string ToMd5Str(string input, string? salt = null)
        {
            var result = MD5.HashData(Encoding.UTF8.GetBytes(input + (salt ?? string.Empty)));
            var strResult = BitConverter.ToString(result);
            return strResult.Replace("-", string.Empty);
        }
    }
}

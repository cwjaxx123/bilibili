﻿using Microsoft.AspNetCore.Mvc;
using Model.Enum;

namespace Model
{
    /// <summary>
    /// 统一结果返回
    /// </summary> 
    /// <remarks>
    /// 统一结果返回 构造函数
    /// <para>默认成功2000 无提示信息</para>
    /// </remarks>
    /// <param name="data">数据</param>
    /// <param name="code">业务状态码 默认成功2000</param>
    /// <param name="errorMsg">错误代码枚举 默认为null</param>
    public class ResResult(dynamic data, ResultEnum code = ResultEnum.Suceess, ErrorCodeEnum? errorMsg = null)
    {
        /// <summary>
        /// 业务状态码
        /// <para>和http响应码要区分开</para>
        /// </summary>
        public ResultEnum Code { set; get; } = code;

        /// <summary>
        /// 提示信息
        /// </summary>
        public string? Msg { set; get; } = errorMsg is not null ? nameof(errorMsg) : null;

        /// <summary>
        /// 错误代码枚举
        /// </summary>
        public ErrorCodeEnum? ErrorCode { set; get; } = errorMsg;

        /// <summary>
        /// 数据
        /// </summary>
        public dynamic Data { set; get; } = data;


    }
}

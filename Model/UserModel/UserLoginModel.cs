﻿using Common;

using Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UserModel
{
    /// <summary>
    /// 用户登录表单
    /// <para>手机号和邮箱必须一个不为空</para>
    /// </summary> 
    public class UserLoginModel : IModelValidationInterface
    {
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string? Email { set; get; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string? Phone { set; get; }

        /// <summary>
        /// 邮箱或手机号密码
        /// </summary>
        public string Password { set; get; } = null!;

        /// <summary>
        /// 登录方式
        /// </summary>
        public LoginMethodEnum LoginMethod { set; get; }

        /// <summary>
        /// 表单验证
        /// </summary>
        bool IModelValidationInterface.IsVerify => !string.IsNullOrWhiteSpace(Email) || !string.IsNullOrWhiteSpace(Phone);
    }
}

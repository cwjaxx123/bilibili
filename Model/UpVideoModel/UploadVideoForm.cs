﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UpVideoModel
{
    /// <summary>
    /// 视频上传表单
    /// </summary>
    public class UploadVideoForm
    {
        /// <summary>
        /// 上传者的id
        /// <para>后端通过解析jwt的payload获取 无需前端设置</para>
        /// </summary>
     //   [Range(0,long.MaxValue)]
        public Guid? UserId { get; set; } =null;

        public string Title { set; get; } = null!;

        public string? Source { set; get; }
        /// <summary>
        /// 分区id
        /// </summary>
       // [Range(1,long.MaxValue)]
        public Guid Subarea { set; get; }
        /// <summary>
        /// 类别id
        /// </summary>
       // [Range(1,long.MaxValue)]
        public Guid Category {  set; get; }

        /// <summary>
        /// 视频简介
        /// </summary>
        public string? Synopsis { set; get; }

        /// <summary>
        /// 标签集合
        /// </summary>
      
        public string[]? Tags {  set; get; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UpVideoModel
{
    /// <summary>
    /// 上传视频封面切片对象
    /// </summary>
    public class UpVideoCoverChunkForm
    {

        [Range(1,long.MaxValue)]
        public long Index { set; get; }
        /// <summary>
        /// 封面切片唯一标识 md5
        /// </summary>
         public string Md5 { set; get; } =null!;
        /// <summary>
        /// 封面的MD5
        /// </summary>
        public string CoverMd5 { set; get; }=null!;
        /// <summary>
        /// 封面切片所属的投稿表单的md5
        /// </summary>
         public string FormMd5 { set; get; } =null!;
    }
}

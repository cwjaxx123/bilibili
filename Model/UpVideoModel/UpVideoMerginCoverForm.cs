﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UpVideoModel
{
    /// <summary>
    /// 合并视频封面接口表单
    /// </summary>
    public class UpVideoMerginCoverForm
    {
        /// <summary>
        /// 封面所属投稿视频的md5
        /// </summary>
        public string FormMd5 { get; set; } = null!;
        /// <summary>
        /// 封面图片的md5
        /// </summary>
        public string Md5 { get; set; } = null!;
        /// <summary>
        /// 封面切片总数量
        /// </summary>
        [Range(1, long.MaxValue)]
        public long ChunkCount { get; set; }
        /// <summary>
        /// 图片类型 带.
        /// </summary>
        public string PicType { get; set; } = null!;
    }
}

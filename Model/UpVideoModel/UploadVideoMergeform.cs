﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UpVideoModel
{
    /// <summary>
    /// 合并视频切片的表单
    /// </summary>
    public class UploadVideoMergeform
    {
        /// <summary>
        /// 视频所属表单
        /// </summary>
         public string FormMd5 { get; set; } = null!;
        /// <summary>
        /// 视频MD5
        /// </summary>
         public string Md5 { get; set; } = null!;
        /// <summary>
        /// 视频切片数量
        /// </summary>
        [Range(1, long.MaxValue)]
        public long ChunkCount { get; set; }
        /// <summary>
        /// 视频类型 带.
        /// </summary>
        public string VideoType { get; set; } = null!;

        /// <summary>
        /// 视频文件名
        /// </summary>
        [StringLength(100)]
        public string Name { get; set; } = null!;
        /// <summary>
        /// 视频时长 单位s
        /// <para>最短 5s</para>
        /// </summary>
        [Range(5, long.MaxValue)]
        public long Duration { set; get; }
        /// <summary>
        /// 视频容量大小 最大5*1024
        /// <para>单位mb</para>
        /// </summary>   
        [Range(0, 5 * 1024)]
        public int Size { set; get; }
        /// <summary>
        /// 在视频列表中的排序
        /// </summary>    
        [Range(1, 200)]
        public long Index { get; set; }
        /// <summary>
        /// 视频存储在服务器的路径
        /// <para>不需要前端传过来</para>
        /// </summary>
        public string? VideoPath { get; set; }
    }

}

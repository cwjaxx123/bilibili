﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UpVideoModel
{
    /// <summary>
    /// 文件切片表单
    /// </summary>
    public class UploadVideoFileChunkForm
    {
        [Range(1, long.MaxValue)]
        public long Index { get; set; }
        /// <summary>
        /// 切片的md5
        /// </summary>
        public string Md5 { get; set; } = null!;
        /// <summary>
        /// 切片所属影片的MD5
        /// </summary>
        public string VideoMd5 { get; set; } = null!;
        public string FormMd5 { get; set; } = null!;
    }
}

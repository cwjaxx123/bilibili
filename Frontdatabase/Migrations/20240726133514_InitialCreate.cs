﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Frontdatabase.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bilibli_user",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    NickName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "用户昵称 唯一索引"),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "用户邮箱"),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "手机号码"),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "用户密码"),
                    Salt = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "盐 配合密码使用"),
                    Grade = table.Column<int>(type: "int", nullable: false, comment: "用户等级"),
                    Profile = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "用户头像图片地址"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "个性签名"),
                    IsLocked = table.Column<bool>(type: "bit", nullable: false, comment: "用户是否被封禁"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "用户是否注销"),
                    FansCount = table.Column<long>(type: "bigint", nullable: false, comment: "粉丝数"),
                    AttentionCount = table.Column<long>(type: "bigint", nullable: false, comment: "关注数"),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bilibli_user", x => x.Id);
                },
                comment: "用户表");

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    Name = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false, comment: "类别名 如:电影,电视剧,纪录片"),
                    Route = table.Column<string>(type: "nvarchar(450)", nullable: false, comment: "页面路由"),
                    Isgeneral = table.Column<bool>(type: "bit", nullable: false, comment: "是不是普通的类别 默认是;如: 电影 电视剧 番剧 就是不是普通类别;游戏 音乐 知识等就是普通类别"),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "假删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                },
                comment: "类别表 如:电影 电视剧 纪录片");

            migrationBuilder.CreateTable(
                name: "VideoCompilations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    Synopsis = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "视频合集简介"),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "假删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoCompilations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubAreas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    Name = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false, comment: "分区名"),
                    Route = table.Column<string>(type: "nvarchar(450)", nullable: false, comment: "页面路由"),
                    Synopsis = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "分区简介"),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "假删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubAreas_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "分区表 一个分区对应一个类别,一个类别对应多个分区");

            migrationBuilder.CreateTable(
                name: "VideoList",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "投稿视频标题"),
                    VideoType = table.Column<int>(type: "int", nullable: false, comment: "投稿视频的类型: 自制 转载"),
                    Cover = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "视频封面 的地址"),
                    Synopsis = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "投稿视频简介"),
                    DanmuCount = table.Column<long>(type: "bigint", nullable: false),
                    Size = table.Column<long>(type: "bigint", nullable: false, comment: "视频总大小 最大200gb "),
                    ZanSize = table.Column<long>(type: "bigint", nullable: false, comment: "点赞数"),
                    WacthCount = table.Column<long>(type: "bigint", nullable: false, comment: "播放量"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VideoCompilationsId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "假删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VideoList_VideoCompilations_VideoCompilationsId",
                        column: x => x.VideoCompilationsId,
                        principalTable: "VideoCompilations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_VideoList_bilibli_user_UserId",
                        column: x => x.UserId,
                        principalTable: "bilibli_user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "视频列表 一个视频必需属于一个视频列表 即使用户只上传一个视频 也要创建一个列表并把视频放入列表中");

            migrationBuilder.CreateTable(
                name: "VideoEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Guid做主键"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "视频文件名"),
                    Duration = table.Column<long>(type: "bigint", nullable: false, comment: "视频时长 单位s 最短5s"),
                    Size = table.Column<long>(type: "bigint", nullable: false, comment: "视频容量大小 最大5*1024Mb"),
                    Index = table.Column<long>(type: "bigint", nullable: false, comment: "视频在视频列表中的排序"),
                    Src = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "视频地址"),
                    VideoListId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Create_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    UpDate_Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "更新时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "假删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VideoEntities_VideoList_VideoListId",
                        column: x => x.VideoListId,
                        principalTable: "VideoList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "单个视频 记录视频的基本信息");

            migrationBuilder.CreateIndex(
                name: "IX_bilibli_user_NickName",
                table: "bilibli_user",
                column: "NickName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Name",
                table: "Categories",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Route",
                table: "Categories",
                column: "Route",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubAreas_CategoryId",
                table: "SubAreas",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubAreas_Name_CategoryId",
                table: "SubAreas",
                columns: new[] { "Name", "CategoryId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubAreas_Route",
                table: "SubAreas",
                column: "Route",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VideoEntities_VideoListId",
                table: "VideoEntities",
                column: "VideoListId");

            migrationBuilder.CreateIndex(
                name: "IX_VideoList_UserId",
                table: "VideoList",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_VideoList_VideoCompilationsId",
                table: "VideoList",
                column: "VideoCompilationsId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubAreas");

            migrationBuilder.DropTable(
                name: "VideoEntities");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "VideoList");

            migrationBuilder.DropTable(
                name: "VideoCompilations");

            migrationBuilder.DropTable(
                name: "bilibli_user");
        }
    }
}

﻿
using Frontdatabase.Entity.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Front_Efcore.Entity.subarea
{
    [Index(nameof(Name),nameof(CategoryId), IsUnique = true),//唯一约束
        Index(nameof(Route), IsUnique = true),
        Comment("分区表 一个分区对应一个类别,一个类别对应多个分区")]  
    /// <summary>
    /// 分区 
    /// <para>一个分区对应一个类别 一个类别对应多个分区</para>
    /// 
    /// </summary>
    public class Subarea : EntityBase
    {

        #region 基本属性
        [ MaxLength(10),Comment("分区名")]
        public string Name { get; set; } = null!;
        [Comment("页面路由")]
        public string Route { set; get; } = null!;
        [MaxLength(100), Comment("分区简介")]
        public string? Synopsis { set; get; }

        #endregion
        #region 外键
        [ForeignKey(nameof(Category))]
        public Guid CategoryId { set; get; }
        public  Category Category { get; set; } = null!;

        #endregion
    }
}

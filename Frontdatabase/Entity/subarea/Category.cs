﻿
using Frontdatabase.Entity.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Front_Efcore.Entity.subarea
{
    [Index(nameof(Name),IsUnique =true),
        Index(nameof(Route), IsUnique = true),
        Comment("类别表 如:电影 电视剧 纪录片")
        ]
    /// <summary>
    /// 类别
    /// <para>电影 电视剧 纪录片</para>
    /// <para>影视 知识 生活</para>
    /// <para>音乐 小说</para>
    /// </summary>
    public class Category : EntityBase
    {
        #region 基本属性
        [ MaxLength(10),Comment("类别名 如:电影,电视剧,纪录片")]
        public string Name { get; set; } = null!;
        [ Comment("页面路由")]
        public string Route { set; get; } = null!;
       
        /// <summary>
        /// 是不是普通的类别 默认是
        /// <para>如: 电影 电视剧 番剧 就是不是普通类别</para>
        /// <para>游戏 音乐 知识等就是普通类别</para>
        /// </summary>
        [Comment("是不是普通的类别 默认是;如: 电影 电视剧 番剧 就是不是普通类别;游戏 音乐 知识等就是普通类别")]
        public bool Isgeneral {  get; set; }=true;

        #endregion

        #region 外键
        public List<Subarea> Subareas { get; set; } = [];

        #endregion
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontdatabase.Entity.Base
{
    /// <summary>
    /// 抽取实体类公共属性形成的基类
    /// </summary>
    public abstract class EntityBase
    {
        [Key,Comment("Guid做主键")]
        public Guid Id { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTime Create_Time { get; set; } = DateTime.Now;
        /// <summary>
        /// 更新时间
        /// </summary>
        [Comment("更新时间")]
        public DateTime UpDate_Time { get; set; }
        /// <summary>
        /// 是否假删除
        /// </summary>
        [Comment("假删除")]
        public bool IsDeleted { get; set; }= false;
    }
}

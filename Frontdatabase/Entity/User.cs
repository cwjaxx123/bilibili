﻿using Frontdatabase.Entity.Base;
using Frontdatabase.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Frontdatabase.Entity
{
    /// <summary>
    /// 用户实体对象
    /// </summary>
    [Table("bilibli_user"), Comment("用户表"), Index(nameof(NickName), IsUnique = true) ]
    public class User : EntityBase
    {
        /// <summary>
        /// 用户昵称
        /// </summary>
        [MinLength(3), MaxLength(20), Required, Comment("用户昵称 唯一索引"),]
        public string NickName { set; get; } = null!;

        /// <summary>
        /// 用户邮箱
        /// </summary>
        [Comment("用户邮箱")]
        public string? Email { set; get; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [Comment("手机号码")]
        public string? Phone { set; get; }
        /// <summary>
        /// 用户密码
        /// </summary>
        [Comment("用户密码"), JsonIgnore]
        public string Password { set; get; } = null!;
        /// <summary>
        /// 盐 配合密码使用
        /// </summary>
        [Comment("盐 配合密码使用"), JsonIgnore]
        public string Salt { set; get; } = string.Empty;
        /// <summary>
        /// 用户等级
        /// </summary>
        [Comment("用户等级")]
        public GradeEnum Grade { set; get; } = GradeEnum.Zeroth;
        /// <summary>
        /// 用户头像图片地址
        /// </summary>
        [Comment("用户头像图片地址")]
        public string? Profile { set; get; }
        /// <summary>
        /// 个性签名
        /// </summary>
        [Comment("个性签名"), MaxLength(100)]
        public string? Remark { set; get; } = "这个人很懒,什么也没留下";
        /// <summary>
        /// 用户是否被封禁
        /// </summary>
        [Comment("用户是否被封禁")]
        public bool IsLocked { set; get; } = false;

        /// <summary>
        /// 用户是否注销
        /// </summary>
        [Comment("用户是否注销")]
        public new bool IsDeleted { set; get; } = false;
        /// 粉丝数
        /// </summary>
        [Comment("粉丝数"), MinLength(0)]
        public long FansCount { set; get; } = 0;
        /// <summary>
        /// 关注数
        /// </summary>
        [Comment("关注数"), MinLength(0)]
        public long AttentionCount { set; get; } = 0;

    }
}

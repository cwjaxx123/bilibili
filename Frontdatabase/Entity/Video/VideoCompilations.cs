﻿using Frontdatabase.Entity.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontdatabase.Entity.Video
{
    /// <summary>
    ///  视频合集 和列表(VideoList)不一样
    ///  <para>一个视频合集可以有多个视频列表</para>
    ///  <para>一个视频列表下面可以有多个视频</para>
    /// </summary>
    public class VideoCompilations : EntityBase
    {
        /// <summary>
        /// 合集简介
        /// </summary>
        [Comment("视频合集简介")]
        public string? Synopsis { get; set; }
    }
}

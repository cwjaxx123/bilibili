﻿using Frontdatabase.Entity.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontdatabase.Entity.Video
{
    [Comment("单个视频 记录视频的基本信息")]
    /// <summary>
    /// 单个视频
    /// </summary>
    public class VideoEntity : EntityBase
    {
        #region 基本属性
        /// <summary>
        /// 视频文件名
        /// </summary>
        [ MaxLength(100), MinLength(1), Comment("视频文件名")]
        public string Name { get; set; } = null!;
        /// <summary>
        /// 视频时长 单位s
        /// <para>最短5s</para>
        /// </summary>
        [Range(5 , long.MaxValue), Comment("视频时长 单位s 最短5s")]
        public long Duration { set; get; }
        /// <summary>
        /// 视频容量大小 最大5*1024
        /// <para>单位mb</para>
        /// </summary>   
        [Range(0, 5 * 1024), Comment("视频容量大小 最大5*1024Mb")]
        public long Size { set; get; }
        /// <summary>
        /// 在视频列表中的排序
        /// </summary>    
        [Range(1, 200), Comment("视频在视频列表中的排序")]
        public long Index { get; set; }
        /// <summary>
        /// 视频地址
        /// </summary>
        [Comment("视频地址")]
        public string Src { get; set; } = null!;

        #endregion

        #region 外键
        [ ForeignKey(nameof(VideoList))]
        public Guid VideoListId { get; set; }
        public VideoList VideoList { get; set; } = null!;
        #endregion
    }
}

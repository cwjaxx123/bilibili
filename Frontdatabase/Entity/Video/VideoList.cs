﻿using Frontdatabase.Entity.Base;
using Microsoft.EntityFrameworkCore;
using Model.Enum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Frontdatabase.Entity.Video
{
    /// <summary>
    /// 视频列表
    /// <para>一个视频必需属于一个视频列表</para>
    /// <para>即使用户只上传一个视频 也要创建一个列表并把视频放入列表中</para>
    /// </summary>
    [Comment($@"视频列表 一个视频必需属于一个视频列表 即使用户只上传一个视频 也要创建一个列表并把视频放入列表中")]
    public class VideoList : EntityBase
    {
        #region 基本属性

        /// <summary>
        /// 投稿视频标题
        /// </summary>
        [MaxLength(100), Comment("投稿视频标题")]
        public string Title { get; set; } = null!;
        /// <summary>
        /// 投稿视频的类型: 自制 转载
        /// </summary>
        [Comment("投稿视频的类型: 自制 转载")]
        public UploadvideoTypeEnum VideoType { get; set; }

        /// <summary>
        /// 视频封面 的地址
        /// </summary>
        [Comment("视频封面 的地址")]
        public string Cover { get; set; } = null!;
        /// <summary>
        /// 投稿视频简介
        /// </summary>
        [Range(0, 1000), Comment("投稿视频简介")]
        public string? Synopsis { set; get; }
        #endregion

        #region 辅助作用的属性(可以通过多表查询得到 但为了效率而设置这些属性)
        [Range(0, long.MaxValue)]
        public long DanmuCount { set; get; }

        /// <summary>
        /// 视频总大小 最大20gb 
        /// <para>单位mb</para>
        /// </summary>
        ///  [Required]
        [Range(0, 20 * 1024), Comment("视频总大小 最大200gb ")]
        public long Size { set; get; }

        /// <summary>
        /// 点赞数
        /// </summary>
        [Range(0, long.MaxValue), Comment("点赞数")]
        public long ZanSize { set; get; }
        /// <summary>
        /// 播放量
        /// </summary>
        [Range(0, long.MaxValue), Comment("播放量")]
        public long WacthCount { set; get; }
        #endregion


        #region 外键

        /// <summary>
        /// 每个视频
        /// </summary>
        public List<VideoEntity> VideoEntities { get; set; } = [];
        /// <summary>
        /// 发布者
        /// </summary>
        [ForeignKey(nameof(User))]
        public Guid UserId { get; set; }

        /// <summary>
        /// 投稿的用户
        /// </summary>
        [Comment("投稿的用户")]
        public User User { get; set; } = null!;

        /// <summary>
        /// 视频合集的id
        /// </summary>
        [ForeignKey(nameof(Compilations))]
        public Guid? VideoCompilationsId { get; set; }
        public VideoCompilations? Compilations { get; set; }


        #endregion
    }
}

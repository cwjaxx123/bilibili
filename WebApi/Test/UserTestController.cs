﻿using Common.Helper;
using Frontdatabase;
using Frontdatabase.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Test
{
    /// <summary>
    /// 用户测试
    /// </summary>
    /// <param name="_context"></param>
    /// <param name="_jwtHelper"></param>
    [ApiController, Route("test/[controller]/[action]")]
    public class UserTestController(FrontDbContext _context, JwtHelper _jwtHelper) : ControllerBase
    {
        /// <summary>
        /// 创建几个用户
        /// </summary>
        /// <returns></returns>
        public async Task<List<User>> CreateUsers()
        {
            for (int i = 0; i < 10; i++)
            {
                var u = new User
                {
                    NickName = $"bilibili_{i + 1}",
                    Password = MD5Helper.ToMd5Str("123456", null),
                    Email = $"12345678{i}@qq.com"
                };
                _context.Add(u);
            }
          await  _context.SaveChangesAsync();
            var list = await _context.Users.ToListAsync();
            return list;
        }
        /// <summary>
        /// 测试token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string token()
        {
            return _jwtHelper.CreateToken(null);
        }
    }

}

﻿using Front_Efcore.Entity.subarea;
using Frontdatabase;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Test
{
    /// <summary>
    /// 测试分区
    /// </summary>
    /// <param name="_context"></param>
    [ApiController, Route("test/[controller]/[action]")]
    public class CategoryTestController (FrontDbContext _context)
        : ControllerBase
    {
        /// <summary>
        /// 添加测试分区
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> addSubara()
        {
            {
                Category s1 = new() { Name = "影视", Route = "/v/cinephile" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "影视杂谈", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "影视剪辑", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "短片", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "预告·资讯", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "小剧场", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "动画", Route = "/v/douga" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "MAD·AMV", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "手办·模玩", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "NMD·3D", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "特摄", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "短片·手书", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "动漫杂谈", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "配音", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "鬼畜", Route = "/v/kichiku" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "鬼畜调教", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "教程演示", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "教程音MAD", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "人力VOCALOID", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "鬼畜剧场", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "舞蹈", Route = "/v/dance" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "宅舞", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "网红舞", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "街舞", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "舞蹈综合", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "明星舞蹈", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "舞蹈教程", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "国风舞蹈", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "娱乐", Route = "/v/ent" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "综艺", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "娱乐杂谈", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "粉丝创作", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "明星综合", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "科技", Route = "/v/tech" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "数码", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "极客DIY", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "软件应用", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "计算机技术", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "科工机械", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "美食", Route = "/v/food" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "美食制作", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "美食侦探", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "美食记录", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "美食评测", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "田园美食", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "汽车", Route = "/v/car" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "赛车", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "摩托车", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "改装玩车", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "购车攻略", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "新能源车", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "汽车生活", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "房车", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "运动", Route = "/v/sports" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "篮球", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "运动文化", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "足球", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "运动综合", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "健身", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "竞技体育", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "游戏", Route = "/v/game" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "单机游戏", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "桌游棋牌", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "游戏赛事", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "电子竞技", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "GMV", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "手机游戏", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "网络游戏", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "音乐", Route = "/v/music" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "原创音乐", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "音乐现场", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "音乐综合", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "翻唱", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "演奏", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "演唱会", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "音乐教学", Category = s1 });

            }

            {
                Category s1 = new Category { Name = "知识", Route = "/v/knowledge" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "科学·科普", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "校园学习", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "职业职场", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "人文历史", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "财经商业", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "野生技能协会", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "资讯", Route = "/v/information" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "科技发布会", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "社会", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "明星", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "新闻", Category = s1 });

            }
            {
                Category s1 = new Category { Name = "生活", Route = "/v/life" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "搞笑", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "家居房产", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "亲子", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "手工", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/e", Name = "出行", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/f", Name = "绘画", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/g", Name = "三农", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/i", Name = "日常", Category = s1 });


            }
            {
                Category s1 = new Category { Name = "时尚", Route = "/v/fashion" };
                _context.Categories.Add(s1);
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/a", Name = "美妆护肤", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/b", Name = "cosplay", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/c", Name = "穿搭", Category = s1 });
                _context.SubAreas.Add(new Subarea { Route = s1.Route + "/d", Name = "时尚潮流", Category = s1 });

            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                Console.WriteLine("发送错误");
            }
            return Ok(await _context.Categories.Include(c => c.Subareas).ToListAsync());
        }
    }
}

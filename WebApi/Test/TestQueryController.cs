﻿using Microsoft.AspNetCore.Mvc;

namespace WebApi.Test
{
    /// <summary>
    /// 测试查询
    /// </summary>
    [ApiController,Route("api/[controller]/[action]")]
    public class TestQueryController:ControllerBase
    {

        public async Task<IActionResult> t1()
        {
            return Ok();
        }
    }
  
}

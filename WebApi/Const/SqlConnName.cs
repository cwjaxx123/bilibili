﻿namespace WebApi.Const
{
    /// <summary>
    /// 数据库链接字符串的名称
    /// </summary>
    public record SqlConnName
    {
        /// <summary>
        /// mysql数据库
        /// </summary>
        public static readonly string Mysql=nameof(MySql);

        /// <summary>
        /// sqlserver数据库
        /// </summary>
        public static readonly string SqlServer=nameof(SqlServer);
    }
}

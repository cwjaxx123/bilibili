using WebApi.Setup;
using WebApi.Steup;
using WebFront;

/*前台项目的api接口*/



var builder = WebApplication.CreateBuilder(args);

#region 组件注入

// 设置mvc
builder.Services.AddController(builder);

// 设置跨域
builder.Services.AddCors(builder);

// 注入数据库连接池
builder.Services.AddDbContext(builder, true);

//使用jwt权限认证
builder.Services.AddJwt(builder);

// 使用redis
builder.Services.AddRedis(builder);

// 注入AutoMapper
builder.Services.AddAutoMapper(builder);

// 分别注入各个模块的服务
builder.Services.AddWebFrontService();

#endregion


var app = builder.Build();


#region 组件配置
// 开启跨域
app.UseCors("AllowAllOrigins");


app.UseRouting();

//身份验证
app.UseAuthentication();

//权限验证
app.UseAuthorization();

//控制器
app.MapControllers();

#region 添加各个模块所需的中间件
app.UseDanmuWebSocket();
#endregion
#endregion

app.Run();


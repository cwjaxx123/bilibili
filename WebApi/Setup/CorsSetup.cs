﻿namespace WebApi.Setup
{
    /// <summary>
    /// Cors配置
    /// </summary>
    public static class CorsSetup
    {
        /// <summary>
        /// Cors配置 依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="builder"></param>
        public static void AddCors(this IServiceCollection Services, WebApplicationBuilder builder) {
            Services.AddCors(op =>
            {
                op.AddPolicy("AllowAllOrigins", policy =>
                {
                    policy.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                });
            });
        }
    }
}

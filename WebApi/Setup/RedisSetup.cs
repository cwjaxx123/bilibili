﻿using Common.Helper;

namespace WebApi.Setup
{
    /// <summary>
    /// redis配置
    /// </summary>
    public static class RedisSetup
    {
        /// <summary>
        /// Redis配置依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="builder"></param>
        public static void AddRedis(this IServiceCollection Services, WebApplicationBuilder builder)
        {
            //redis缓存
            var section = builder.Configuration.GetSection("Redis:Default");
            //连接字符串
            string _connectionString = section.GetSection("Connection").Value;
            //实例名称
            string _instanceName = section.GetSection("InstanceName").Value;
            //默认数据库 
            int _defaultDB = int.Parse(section.GetSection("DefaultDB").Value ?? 0.ToString());
            Services.AddSingleton(new RedisHelper(_connectionString!, _instanceName!, _defaultDB));

        }
    }
}

﻿using WebFront.AutoMapper;

namespace WebApi.Setup
{
    /// <summary>
    /// AutoMapper配置
    /// </summary>
    public static class AutoMapperSetup
    {
        /// <summary>
        /// automapper配置依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="builder"></param>
        public static void AddAutoMapper(this IServiceCollection Services, WebApplicationBuilder builder)
        {
            Services.AddAutoMapper(op =>
            {
                op.AddProfile<UpVideoMapperProfile>();
            });
        }
        }
}

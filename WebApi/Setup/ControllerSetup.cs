﻿using System.Text.Json.Serialization;

namespace WebApi.Setup
{
    /// <summary>
    /// mvc配置
    /// </summary>
    public static class ControllerSetup
    {
        /// <summary>
        /// Mvc配置 依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="builder"></param>
        public static void AddController(this IServiceCollection Services, WebApplicationBuilder builder)
        {
            Services.AddControllers()
        //json格式化配置
        .AddJsonOptions(op =>
        {
            //不返回null
            op.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            //忽略efcore循环引用
            op.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        });
        }
    }
}

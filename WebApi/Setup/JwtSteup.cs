﻿using System.IdentityModel.Tokens.Jwt;
using Common.Helper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebApi.Steup
{
    /// <summary>
    /// jwt配置
    /// </summary>
    public static class JwtSetup
    {
        /// <summary>
        /// jwt配置依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <returns></returns>
        public static void AddJwt(this IServiceCollection Services, WebApplicationBuilder builder)
        {
            Services.AddSingleton<JwtHelper>();
            Services.AddAuthentication(op =>
            op.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(
                jwt =>
                {

                    jwt.SaveToken = true;
                    jwt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true, //是否验证Issuer
                        ValidIssuer = builder.Configuration["Jwt:Issuer"], //发行人Issuer
                        ValidateAudience = true, //是否验证Audience
                        ValidAudience = builder.Configuration["Jwt:Audience"], //订阅人Audience
                        ValidateIssuerSigningKey = true, //是否验证SecurityKey
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:SecretKey"]!)), //SecurityKey
                                                                                                                                      //ValidateLifetime = true, //是否验证失效时间
                                                                                                                                      //ClockSkew = TimeSpan.FromSeconds(30), //过期时间容错值，解决服务器端时间不同步问题（秒）
                        RequireExpirationTime = true,
                    };
                });
            
        }
    }
}

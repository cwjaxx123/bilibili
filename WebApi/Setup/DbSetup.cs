﻿using Microsoft.EntityFrameworkCore;
using WebApi.Const;

namespace WebApi.Setup
{
    /// <summary>
    /// efcore数据库配置
    /// </summary>
    public static class DbSetup
    {
        /// <summary>
        /// efcore数据库配置 依赖注入
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="builder"></param>
        /// <param name="isPool">是否启用数据库连接池 默认是</param>
        public static void AddDbContext(this IServiceCollection Services, WebApplicationBuilder builder, bool isPool = true)
        {
            if (isPool)
            {
                Services.AddDbContextPool<Frontdatabase.FrontDbContext>(op =>
                {
                    op.UseSqlServer(builder.Configuration.GetConnectionString(SqlConnName.SqlServer) ?? string.Empty);
                });
            }
            else
            {
                Services.AddDbContext<Frontdatabase.FrontDbContext>(op =>
                {
                    op.UseSqlServer(builder.Configuration.GetConnectionString(SqlConnName.SqlServer) ?? string.Empty);

                });
            }
        }
    }
}
